# Statuspage Plugin for MSM

## MSM-Statuspage Integration

This plugin allows you to **View** all messages associated with a specific incident as well as **Update** an existing incident on Statuspage via MSM.

## Statuspage Integration

**Settings needed from Statuspage:**

Within Statuspage follow the steps outlined below:

1. Navigate to statuspage.io
2. Signup
3. Create a new incident by clicking **Incidents** tab then click **New Incident** button.
4. Another way to get **User API Key** is to click on the user avatar on the top right corner of the page, then click **Manage account**.
5. Once the page is loaded click API tab then **User API Key** will be loaded.


## Compatible Versions

| Plugin  | MSM       |Statuspage|
|---------|-----------|----------|
| 1.0.0   | 14.15.0   | v1.0.0   |
| 1.0.8   | 15.1+     | v1.0.0   |


## Installation

Please see your MSM documentation for information on how to install plugins.

Once the plugin has been installed you will need to configure the following settings within the plugin page:

+ *User Api Key*: The user API key.

We recommend that you create a new user within Statuspage.

## Usage

The plugin can be launched from the quick menu after you load a new or existing request.

## Contributing

We welcome all feedback including feature requests and bug reports. Please raise these as issues on GitHub. If you would like to contribute to the project please fork the repository and issue a pull request.
